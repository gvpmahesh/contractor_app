class CreateEvents < ActiveRecord::Migration[7.0]
  def change
    create_table :events do |t|
      t.string :name, null: false
      t.integer :reference_id, null: false
      t.jsonb :payload, default: {}

      t.timestamps
    end

    add_index :events, [:name, :reference_id], unique: true
  end
end
