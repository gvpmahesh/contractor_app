class CreateInvoices < ActiveRecord::Migration[7.0]
  def change
    create_table :invoices do |t|
      t.text :description, null: false
      t.bigint :amount_cents, null: false
      t.string :amount_currency, null: false

      t.timestamps
    end
  end
end
