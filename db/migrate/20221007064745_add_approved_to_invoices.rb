class AddApprovedToInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :approved_at, :datetime
    add_column :invoices, :rejected_at, :datetime
    add_column :invoices, :payment_id, :integer
  end
end
