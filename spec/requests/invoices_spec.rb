require 'rails_helper'

RSpec.describe "/invoices", type: :request do
  let(:valid_attributes) {
    { description: "MyString", amount: 1, amount_currency: "USD" }
  }

  let(:invalid_attributes) {
    { description: nil }
  }

  describe "GET /index" do
    it "renders a successful response" do
      Invoice.create! valid_attributes
      get invoices_url
      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it "renders a successful response" do
      invoice = Invoice.create! valid_attributes
      get invoice_url(invoice)
      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it "renders a successful response" do
      get new_invoice_url
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Invoice" do
        expect {
          post invoices_url, params: { invoice_form: valid_attributes }
        }.to change(Invoice, :count).by(1)
      end

      it "redirects to the created invoice" do
        post invoices_url, params: { invoice_form: valid_attributes }
        expect(response).to redirect_to(invoice_url(Invoice.last))
      end
    end

    context "with invalid parameters" do
      it "does not create a new Invoice" do
        expect {
          post invoices_url, params: { invoice_form: invalid_attributes }
        }.to change(Invoice, :count).by(0)
      end
    end
  end
end
