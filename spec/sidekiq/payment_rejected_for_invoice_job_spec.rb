require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe PaymentRejectedForInvoiceJob, type: :job do
  describe "#perform" do
    let!(:invoice) { create(:invoice)}
    let(:payload) {
      "{\"id\":15,\"name\":\"payment_rejected\",\"payload\":{\"id\":43,\"created_at\":\"2022-10-10T10:11:59.592Z\",\"updated_at\":\"2022-10-10T10:12:06.264Z\",\"approved_at\":null,\"description\":\"reject this\",\"rejected_at\":\"2022-10-10T10:12:06.256Z\",\"amount_cents\":2000000,\"amount_currency\":\"EUR\",\"invoice_reference_id\":#{invoice.id}},\"payment_id\":43,\"created_at\":\"2022-10-10T10:12:06.274Z\",\"updated_at\":\"2022-10-10T10:12:06.274Z\"}"
    }

    it 'calls MarkInvoiceAsApproved service' do
      Sidekiq::Testing.inline! do
        expect(MarkInvoiceAsRejected).to receive(:new).with(JSON.parse(payload)).and_call_original

        described_class.new.perform(payload)
      end
    end
  end
end
