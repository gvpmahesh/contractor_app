require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe PaymentApprovedForInvoiceJob, type: :job do
  describe "#perform" do
    let!(:invoice) { create(:invoice)}
    let(:payload) {
      "{\"id\":13,\"name\":\"payment_approved\",\"payload\":{\"id\":42,\"created_at\":\"2022-10-10T10:01:55.148Z\",\"updated_at\":\"2022-10-10T10:02:00.685Z\",\"approved_at\":\"2022-10-10T10:02:00.678Z\",\"description\":\"cool beans\",\"rejected_at\":null,\"amount_cents\":250000,\"amount_currency\":\"EUR\",\"invoice_reference_id\":#{invoice.id}},\"payment_id\":42,\"created_at\":\"2022-10-10T10:02:00.694Z\",\"updated_at\":\"2022-10-10T10:02:00.694Z\"}"
    }

    it 'calls MarkInvoiceAsApproved service' do
      Sidekiq::Testing.inline! do
        expect(MarkInvoiceAsApproved).to receive(:new).with(JSON.parse(payload)).and_call_original

        described_class.new.perform(payload)
      end
    end
  end
end
