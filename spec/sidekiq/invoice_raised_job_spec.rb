require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe InvoiceRaisedJob, type: :job do
  let!(:invoice) { create(:invoice) }
  let!(:event) { create(:event, name: 'invoice_raised', reference_id: invoice.id, payload: invoice.attributes) }

  it "writes to kafka topic if cache is not set" do
    Sidekiq::Testing.inline! do
      expect(Karafka.producer).to receive(:produce_sync)
      InvoiceRaisedJob.perform_async(event.id)
    end
  end

  it "does not write to kafka topic if cache is set" do
    Sidekiq::Testing.inline! do
      $redis.set("invoice_raised:#{event.id}", 1)
      expect(Karafka.producer).not_to receive(:produce_sync)

      InvoiceRaisedJob.perform_async(event.id)
    end
  end
end
