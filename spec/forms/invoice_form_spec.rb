require "rails_helper"
require 'sidekiq/testing'

RSpec.describe InvoiceForm, type: :form do
  let(:valid_attributes) {
    {
      description: "Invoice description",
      amount: 1000,
      amount_currency: "USD"
    }
  }

  describe "validations" do
    it "is invalid without a description" do
      expect(
        InvoiceForm.new(valid_attributes.merge(description: nil))
      ).not_to be_valid
    end

    it "supports only USD and EUR currencies" do
      expect(InvoiceForm.new(valid_attributes.merge(amount_currency: 'USD'))).to be_valid
      expect(InvoiceForm.new(valid_attributes.merge(amount_currency: 'EUR'))).to be_valid
      expect(InvoiceForm.new(valid_attributes.merge(amount_currency: 'GBP'))).not_to be_valid
    end

    it "is invalid with negative amount" do
      expect(
        InvoiceForm.new(valid_attributes.merge(amount: -1000))
      ).not_to be_valid
    end

    it "is invalid with zero amount" do
      expect(
        InvoiceForm.new(valid_attributes.merge(amount: 0))
      ).not_to be_valid
    end

    it "is valid with positive amount" do
      expect(
        InvoiceForm.new(valid_attributes.merge(amount: 1000))
      ).to be_valid
    end
    context "stores fractional values" do
      it do
        invoice_form = InvoiceForm.new(valid_attributes.merge(amount: 123.5))

        expect(invoice_form.save.amount_cents).to eq(12350)
      end
    end
  end

  context "for valid attributes" do
    it "saves the invoice and returns the invoice object" do
      invoice_form = InvoiceForm.new(valid_attributes)

      expect(invoice_form.save).to be_a(Invoice)
      expect(invoice_form.save.persisted?).to be(true)
    end

    it "the saved invoice approved attribute is false" do
      invoice_form = InvoiceForm.new(valid_attributes)

      expect(invoice_form.save.approved_at).to be(nil)
    end

    it "creates an event with name invoice_raised" do
      invoice_form = InvoiceForm.new(valid_attributes)

      saved_invoice = invoice_form.save
      event = Event.find_by(name: 'invoice_raised', reference_id: saved_invoice.id)

      expect(event.payload).to eq(saved_invoice.attributes.as_json)
    end

    it "enqueues a job InvoiceRaised with event id" do
      allow(InvoiceRaisedJob).to receive(:perform_async)

      invoice_form = InvoiceForm.new(valid_attributes)
      saved_invoice = invoice_form.save
      event = Event.find_by(name: 'invoice_raised', reference_id: saved_invoice.id)

      expect(InvoiceRaisedJob).to have_received(:perform_async).with(event.id)
    end
  end
end
