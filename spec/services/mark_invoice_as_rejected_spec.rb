require 'rails_helper'

describe MarkInvoiceAsRejected, type: :service do
  let!(:invoice) { create(:invoice) }

  let(:data) {
    {
      "id" => 56,
      "name" => "payment_rejected",
      "payload" => {
        "id" => 82,
        "approved_at" => nil,
        "rejected_at" => "2022-10-09T13:22:10.000Z",
        "invoice_reference_id" => invoice.id,
        "description" => "request for invoice",
        "amount_cents" => 100_000,
        "amount_currency" => "USD",
        "created_at" => "2022-10-09T13:22:10.000Z",
        "updated_at" => "2022-10-09T13:22:10.000Z"
      },
      "payment_id" => 82,
      "created_at" => "2022-10-09T13:22:10.000Z",
      "updated_at" => "2022-10-09T13:22:10.000Z"
    }
  }

  it "updates the invoice rejected_at attribute" do
    described_class.new(data).save!

    invoice.reload

    expect(invoice.rejected_at).to eq(data['payload']['rejected_at'])
    expect(invoice.payment_id).to eq(data['payment_id'])
  end


  it "raises an error if invoice is not found" do
    data['payload']['invoice_reference_id'] = 9999

    expect { described_class.new(data).save! }.to raise_error(ActiveRecord::RecordNotFound)
  end
end
