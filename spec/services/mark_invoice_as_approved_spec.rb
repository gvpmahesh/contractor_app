require 'rails_helper'

describe MarkInvoiceAsApproved, type: :service do
  let!(:invoice) { create(:invoice) }

  let(:data) {
    {
      "id" => 56,
      "name" => "payment_approved",
      "payload" => {
        "id" => 82,
        "approved_at" => "2022-10-09T13:22:10.000Z",
        "rejected_at" => nil,
        "invoice_reference_id" => invoice.id,
        "description" => "request for invoice",
        "amount_cents" => 100_000,
        "amount_currency" => "USD",
        "created_at" => "2022-10-09T13:22:10.000Z",
        "updated_at" => "2022-10-09T13:22:10.000Z"
      },
      "payment_id" => 82,
      "created_at" => "2022-10-09T13:22:10.000Z",
      "updated_at" => "2022-10-09T13:22:10.000Z"
    }
  }

  it "updates the invoice approved_at attribute" do
    described_class.new(data).save!

    invoice.reload

    expect(invoice.approved_at).to eq(data['payload']['approved_at'])
    expect(invoice.payment_id).to eq(data['payment_id'])
  end
end
