FactoryBot.define do
  factory :event do
    name { "some_event" }
    reference_id { rand(1..100) }
    payload { {} }
  end
end
