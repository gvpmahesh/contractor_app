FactoryBot.define do
  factory :invoice do
    description { "Invoice for the month of september 2022" }
    amount { Money.new(1000, "USD") }
  end
end
