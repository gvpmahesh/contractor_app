require 'rails_helper'

RSpec.describe Event, type: :model do
  describe "validations" do
    it "is invalid without a name" do
      event = build(:event, name: nil)
      expect(event).to be_invalid
    end

    it "is invalid without a reference_id" do
      event = build(:event, reference_id: nil)
      expect(event).to be_invalid
    end

    it "requires a unique name and reference_id" do
      event = create(:event)
      event2 = build(:event, name: event.name, reference_id: event.reference_id)
      expect(event2).to be_invalid
    end
  end
end
