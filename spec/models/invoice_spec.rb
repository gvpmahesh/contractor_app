require "rails_helper"

RSpec.describe Invoice, type: :model do
  describe "validations" do
    it "has a valid factory" do
      expect(build(:invoice)).to be_valid
    end

    it "is invalid without a description" do
      expect(build(:invoice, description: nil)).to_not be_valid
    end

    it "is invalid without an amount" do
      expect(build(:invoice, amount_cents: nil)).to_not be_valid
    end
  end
end
