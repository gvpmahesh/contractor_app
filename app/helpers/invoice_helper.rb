module InvoiceHelper
  def status(invoice)
    if invoice.approved_at
      'approved'
    elsif invoice.rejected_at
      'rejected'
    else
      'pending for approval'
    end
  end
end
