class Invoice < ApplicationRecord
  monetize :amount_cents, allow_nil: false

  validates :description, presence: true
end
