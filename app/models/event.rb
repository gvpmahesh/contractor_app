class Event < ApplicationRecord
  validates :name, presence: true
  validates :reference_id, presence: true
  validates :name, uniqueness: { scope: :reference_id }
end
