class InvoicesController < ApplicationController
  before_action :set_invoice, only: %i[ show ]

  # GET /invoices
  def index
    @invoices = Invoice.all.order(created_at: :desc)
  end

  # GET /invoices/1
  def show
  end

  # GET /invoices/new
  def new
    @invoice = InvoiceForm.new
  end

  # POST /invoices
  def create
    @invoice = InvoiceForm.new(invoice_params)
    respond_to do |format|
      if @invoice.valid?
        @invoice = @invoice.save
        format.html { redirect_to invoice_path(@invoice), notice: "Invoice was successfully raised." }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_invoice
      @invoice = Invoice.find(params[:id])
    end

    def invoice_params
      params.require(:invoice_form).permit(:description, :amount, :amount_currency)
    end
end
