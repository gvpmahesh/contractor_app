# frozen_string_literal: true

class PaymentApprovedForInvoice < ApplicationConsumer
  def consume
    messages.each do |message|
      payload = message.payload['data']

      PaymentApprovedForInvoiceJob.perform_async(payload.to_json)

      mark_as_consumed(message)
    end
  end
end
