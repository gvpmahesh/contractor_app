# frozen_string_literal: true

class PaymentRejectedForInvoice < ApplicationConsumer
  def consume
    messages.each do |message|
      payload = message.payload['data']

      PaymentRejectedForInvoiceJob.perform_async(payload.to_json)

      mark_as_consumed(message)
    end
  end
end
