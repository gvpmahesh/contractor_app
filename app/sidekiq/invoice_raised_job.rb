class InvoiceRaisedJob
  include Sidekiq::Job

  def perform(event_id)
    event = Event.find(event_id)

    return if cache_set?(event)

    Karafka.producer.produce_sync(topic: 'invoice_raised', payload: {data: event.payload}.to_json)

    set_cache(event)
  end

  private

  def set_cache(event)
    $redis.set("invoice_raised:#{event.id}", 1)
  end

  def cache_set?(event)
    $redis.get("invoice_raised:#{event.id}").present?
  end
end
