class PaymentRejectedForInvoiceJob
  include Sidekiq::Job

  def perform(payload)
    payload = JSON.parse(payload)
    MarkInvoiceAsRejected.new(payload).save!
  end
end
