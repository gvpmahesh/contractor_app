class PaymentApprovedForInvoiceJob
  include Sidekiq::Job

  def perform(payload)
    payload = JSON.parse(payload)
    MarkInvoiceAsApproved.new(payload).save!
  end
end
