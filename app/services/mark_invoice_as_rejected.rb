class MarkInvoiceAsRejected
  attr_reader :payload, :invoice, :data, :payment_id

  def initialize(data)
    @data = data
    @payload = data['payload']
    @invoice = Invoice.find_by(id: payload['invoice_reference_id'])
    @payment_id = data['payment_id']
  end

  def save!
    if invoice.blank?
      message = "Invoice with id #{payload['invoice_reference_id']} not found"
      Rails.logger.error(invoice_id: payload['invoice_reference_id'], message: message)
      raise ActiveRecord::RecordNotFound, message
    end

    ApplicationRecord.transaction do
      invoice.update!(rejected_at: payload['rejected_at'], payment_id: payment_id)
      Event.create!(name: 'payment_rejected', payload: data, reference_id: invoice.id)
    end
  end
end
