class MarkInvoiceAsApproved
  attr_reader :payload, :invoice, :data, :payment_id

  def initialize(data)
    @data = data
    @payload = data['payload']
    @invoice = Invoice.find_by(id: payload['invoice_reference_id'])
    @payment_id = data['payment_id']
  end

  def save!
    if invoice.blank?
      Rails.logger.error("Invoice with id #{payload['invoice_reference_id']} not found")
      raise ActiveRecord::RecordNotFound, "Invoice with id #{payload['invoice_reference_id']} not found"
    end

    ApplicationRecord.transaction do
      invoice.update!(approved_at: payload['approved_at'], payment_id: payment_id)
      Event.create!(name: 'payment_approved', payload: data, reference_id: invoice.id)
    end
  end
end
