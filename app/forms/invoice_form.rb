class InvoiceForm
  include ActiveModel::Model

  validates :description, presence: true

  validates :amount, presence: true, numericality: { greater_than: 0 }

  validates :amount_currency, presence: true, inclusion: { in: %w[USD EUR] }

  attr_accessor :description, :amount, :amount_currency

  def save
    ApplicationRecord.transaction do
      invoice = Invoice.new(description: description, amount_cents: amount_cents, amount_currency: amount_currency)
      saved_invoice = invoice.tap { |i| i.save! }
      event = Event.create!(name: 'invoice_raised', reference_id: saved_invoice.id, payload: saved_invoice.attributes)
      InvoiceRaisedJob.perform_async(event.id)
      saved_invoice
    end
  end

  private

  def amount_cents
    amount.to_f * 100
  end
end
