# README

A Simple Contractor App to raise Invoice requests.

### Dependencies

- ruby-3.0.0
- Postgres(latest)
- sidekiq
- redis
- kafka


### Running the app

```
bundle exec sidekiq
docker compose up # to run kafka
bundle exec karafka server
rails s
```

### Assumptions made

- This is not a multi-tenant application.
- The contractor is the only user of the application.
- The contractor can only raise invoices for the same manager.
- Authentication/Authorization is not implemented.
- Cannot edit/delete the invoice once raised.
